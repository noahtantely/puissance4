#!/usr/bin/env python
# coding: utf-8

# In[ ]:


def coup_aleatoire(gril , j):
    '''
    Fonction coup_aleatoire(gril , j):
    Joue un coup aléatoire pour le joueur j.
    On suppose la grille non pleine ,
    condition indispensable pour ne pas se trouver dans une boucle infinie !
    '''
    from random import randint 
    if match_nul(gril) == False : 
        jouer(gril , j , randint(0,6))


# In[1]:


def jeu():
    grille = grille_vide()
    affiche(grille)
    fin = 0
    while fin == 0 jouer(grille,1,int(input("Ou voulez-vous mettre le pion?(numéro de colonne), joueur 1 : ")))
    affiche(grille)
    if victoire(grille,1) == True:
        fin = 1
        return 'Fin de partie, match nul'
    jouer(grille,2,int(input("Ou voulez-vous mettre le pion?(numéro de colonne), joueur 2 : ")))
    affiche(grille)
    if victoire (grille,2) == True:
        fin = 1
        return "Le joueur 2 a gagné !"
    if match_nul(grille) == True:
        fin =1
        return "Fin de partie, match nul"


# In[ ]:


def coup_possible(gril, col):
    '''Fonction : détermine si possible de jouer dans la colonne col.
    Argument : la grille, tableau de 6x7, avec position des pions des jouerus et un entier, le numéro de la colonne entre 0 et 6.
    Renvoie True si possible, False sinon
    Possible de jouer dans la colonne col, s'il existe une case avec la valeur 0 dans cette colonne'''
    colonne = [ligne[col] for ligne in gril]
    if 0 in colonne:
        return True
    else:
        return False


# In[ ]:


assert coup_possible([[0, 1, 1, 1, 1, 2, 2],
 [2, 2, 0, 0, 2, 0, 2],
 [0, 1, 1, 1, 0, 2, 1],
 [2, 2, 1, 1, 2, 0, 2],
 [0, 0, 2, 1, 0, 1, 2],
 [2, 0, 0, 0, 2, 2, 1]], 2) == True

assert coup_possible([[2, 1, 0, 2, 0, 0, 1],
 [1, 2, 2, 1, 1, 1, 1],
 [1, 1, 2, 2, 2, 2, 1],
 [1, 2, 1, 2, 1, 2, 0],
 [1, 1, 0, 0, 1, 2, 1],
 [0, 2, 0, 2, 0, 1, 0]], 5) == True


# In[ ]:


def jouer(gril, j, col):
    '''Fonction qui joue un coup du joueur j dans la colonne de la grille. Arguments:
    gril est la grille de 6x7 avec les pions des joueurs
    j : 1 pour J2, 2 pour J2.
    col entier entre 0 et 6, désigne colonne non pleine de la grille
    Si j = 1, première case vide de la colonne prend valeur 1
    Si j = 2, première case vide de la colonne prend valeur 2 '''
    for ligne in gril[::-1]: #on parcourt les lignes du bas vers le haut
        if ligne[col] == 0:
            ligne[col] = j
            return gril #on s'arrête dès qu'on croise un 0
    return gril


# In[ ]:


grille = [[0, 1, 0, 1, 1, 2, 2],
 [0, 2, 0, 0, 2, 0, 2],
 [0, 1, 1, 1, 0, 2, 1],
 [2, 2, 1, 1, 2, 0, 2],
 [1, 2, 2, 1, 0, 1, 2],
 [2, 1, 1, 0, 2, 2, 1]]
assert [i[0] for i in jouer(grille,2,0)] == [0, 0, 2, 2, 1, 2]
assert [i[2] for i in jouer(grille,1,2)] == [0, 1, 1, 1, 2, 1]
assert [i[6] for i in jouer(grille,2,6)] == [2,2,1,2,2,1]


# In[3]:


def victoire():
    for ligne in range(len(grille)):
        for colonne in range (len(grille[0])):
            if colonne <= 3:
                if horiz(grille,l,ligne,colonne) == True:
                    return True
            if ligne <=2:
                if vert(grille,j,ligne,colonne) == True or diag_haut(grille,j,ligne,colonne) == True:
                    return True
    return False


