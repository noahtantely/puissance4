#!/usr/bin/env python
# coding: utf-8

# In[7]:


def grille_vide():
    '''Fonction grille_vide():
    La fonction construit un tableau a deux dimensions de taille 6 x 7 :
    6 lignes et 7 colonnes.
    Chaque case contient la valeur 0.
    Pas d'argument.
    Renvoie tableau sous forme de liste de liste. '''
    tableau = [[] for i in range(6)]
    for i in tableau:
        for j in range(7):
            i.append(0)
    return tableau


# In[8]:


assert grille_vide() == [[0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0]]
assert len(grille_vide()[0]) == 7
assert len(grille_vide()) == 6
assert grille_vide()[1][6] == 0


# In[35]:


def affiche(gril) :
    ''' Affiche(gril) : affiche un grille de 6 lignes sur 7 colonnes.
    Prend en argument un tableau de 6x7.
    Une ligne est notée lig et prend une valeur entre 0 et 5.
    La ligne 0 est située en bas.
    Une colonne est notée col et prend une valeur entre 0 et 6, 
    la colonne 0 est située à gauche.
    Dans la grille :
    la valeur 0 représente une case vide, notée '.'
    la valeur 1 représente un pion du joueur 1, notée ♥
    la valeur 2 représente un pion du joueur 2, notée 0   '''
    liste = "|"
    print("  ",0,1,2,3,4,5,6)
    print("  ---------------")
    col = 5
    for i in range(6):
        liste = "|"
        for j in range(7):
            if gril[i][j] == 0:
                liste += '.'
            if gril[i][j] == 1:
                liste += '♥'
            if gril[i][j] == 2:
                liste += 0
            liste += '|'
        print(col,liste)
        col -= 1
    print('  ---------------')
affiche(grille_vide())

    


# In[10]:


assert coup_possible([[0, 1, 1, 1, 1, 2, 2],
 [2, 2, 0, 0, 2, 0, 2],
 [0, 1, 1, 1, 0, 2, 1],
 [2, 2, 1, 1, 2, 0, 2],
 [0, 0, 2, 1, 0, 1, 2],
 [2, 0, 0, 0, 2, 2, 1]], 2) == True

assert coup_possible([[2, 1, 0, 2, 0, 0, 1],
 [1, 2, 2, 1, 1, 1, 1],
 [1, 1, 2, 2, 2, 2, 1],
 [1, 2, 1, 2, 1, 2, 0],
 [1, 1, 0, 0, 1, 2, 1],
 [0, 2, 0, 2, 0, 1, 0]], 5) == True


# In[40]:


pions = [0, 1, 2] #0 : vide, 1 : J1, 2 : J2
tableau = [[] for i in range(6)]
for i in tableau:
    for j in range(7):
        i.append(pions[randint(0,2)]) #on crée un tableau rempli aléatoirement de 0, 1, 2
tableau


# In[13]:


def horiz(gril,j,lig,col) :
    '''
    Détermine si il y a un alignement horizontal de 4 pions du joueur j à partir de la case (lig,col).
    Renvoie True si c'est le cas, False dans le cas contraire.
    '''
    gril = gril[::-1]
    if col>3 :
        False
    else :
        if j==1 : 
            for i in range(col,col+4) : #prend 4 cases à la suite
                if gril[lig][i]!=1 : #à partir de l'index i dans la ligne lig
                    return False #si rencontre une case pas 1 dans les 4 cases suivantes, retourne False
            return True
        if j==2 : 
            for i in range(col,col+4) :
                if gril[lig][i]!=2 :
                    return False
            return True


# In[14]:


assert horiz([[0, 1, 0, 1, 1, 2, 2],
 [2, 2, 0, 0, 2, 0, 2],
 [0, 1, 1, 1, 0, 2, 1],
 [2, 2, 1, 1, 2, 0, 2],
 [1, 2, 0, 1, 1, 1, 1],
 [2, 1, 1, 0, 2, 2, 1]], 1, 1, 3 ) == True


# In[15]:


def vert(gril,j,lig,col):
    '''Détermine si alignement vertical de 4pions de j à partir de la case (lig,col)
    gril : grille avec les pions
    j le joueur 1 ou 2
    lig la ligne, entre 0 et 2
    col la colonne, entre 0 et 6
    Renvoie True si c'est le cas, False sinon '''
    gril = gril[::-1]
    if lig > 2:
        return False
    else:
        for i in range(lig, lig+4):
            if gril[i][col] != j:
                return False
        return True


# In[17]:


def diag_bas(gril,j,lig,col):
    '''Détermine si Alignement diagonal vers le bas de 4 pions à partir de la case (lig,col)
    gril : grille avec les pions en liste de listes
    j : joueur 1 ou 2
    lig : ligne entre O et 2
    col : colonne entre 0 et 6
    Renvoie True si c'est le cas, False sinon '''
    
    dafillé = 0
    if col <= 3:
        for i in range(4):
            if gril[lig-i][col+i] == j: #avance d'une ligne et d'une colonne
                dafillé +=1
    if dafillé == 4:
        return True
    dafillé = 0
    if col >= 3:
        for i in range(4):
            if gril[lig-i][col-i] == j: #avance d'une ligne et recule d'une colonne
                dafillé +=1       
    if dafillé == 4:
        return True
    return False


# In[ ]:


grille1 = [[0, 1, 0, 2, 1, 2, 2],
 [0, 2, 2, 0, 2, 0, 2],
 [0, 2, 1, 1, 0, 2, 1],
 [2, 2, 1, 1, 2, 0, 2],
 [1, 2, 2, 1, 0, 1, 2],
 [2, 1, 1, 0, 2, 2, 1]]


# In[38]:


def match_nul(gril):
    '''
    Renvoie True si partie est nulle, c'est à dire si la ligne du haut est remplie, False sinon '''
    if 0 in gril[0]:
        return False
    return True

